use chrono::Timelike;
use log::{debug, LevelFilter, Metadata, Record, SetLoggerError};

struct SimpleLogger;

impl log::Log for SimpleLogger {
    fn enabled(&self, metadata: &Metadata) -> bool {
        metadata.level() <= log::max_level()
    }

    fn log(&self, record: &Record) {
        if self.enabled(record.metadata()) {
            let now = chrono::offset::Local::now().naive_local();

            let time = format!("[{}:{}:{}]", 
                add_trailing_zero(now.hour()),
                add_trailing_zero(now.minute()),
                add_trailing_zero(now.second()),
            );

            let mut file = record.file()
                .unwrap_or_else(|| "unknown").to_string();

            match file.rfind("/") {
                Some(_) => {
                    file = file.split("/").skip(1)
                        .collect::<Vec<&str>>().join("/");
                },
                None => { },
            }

            let msg = format!("{} {}:{} | {} - {}",
                time,
                file,
                record.line().unwrap_or_else(|| 0),
                record.level(),
                record.args()
            );

            println!("{}", msg);
        }
    }

    fn flush(&self) {}
}

static LOGGER: SimpleLogger = SimpleLogger;

pub fn init(
    level: LevelFilter,
) -> Result<(), SetLoggerError> {
    let res = log::set_logger(&LOGGER)
        .map(|()| log::set_max_level(level));
    debug!("Logging initialized");
    res
}

fn add_trailing_zero(num: u32) -> String {
    if num > 9 {
        format!("{}", num)
    } else {
        format!("0{}", num)
    }
}
