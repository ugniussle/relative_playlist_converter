slint::include_modules!();
mod logging;
use std::{fs, io::{BufRead, Read}, path::Path, process::ExitCode};

use log::{debug, error, info, LevelFilter};
use rfd::FileDialog;

fn get_log_level() -> Option<LevelFilter> {
    let log_level = match get_arg("--log") {
        Some(str) => str,
        None => String::new(),
    };

    match log_level.to_lowercase().as_str() {
        "off" => Some(LevelFilter::Off),
        "error" => Some(LevelFilter::Error),
        "info" => Some(LevelFilter::Info),
        "warn" => Some(LevelFilter::Warn),
        "debug" => Some(LevelFilter::Debug),
        "trace" => Some(LevelFilter::Trace),
        _ => None,
    }
}

fn handle_cmd_run(args: Vec<String>) -> u8 {
    let mut input = String::new();
    let mut output = String::new();
    let mut ignore = String::new();
    let mut exts = String::new();
    let mut convert_windows_sep = false;
    let mut help = false;

    let mut i = 0;
    loop {
        if args.len() <= i {
            break;
        }

        match args[i].to_lowercase().as_str() {
            "--input" => {
                input = args[i + 1].to_string();
                i = i + 2;
            },
            "--output" => {
                output = args[i + 1].to_string();
                i = i + 2;
            },
            "--ignore" => {
                ignore = args[i + 1].to_string();
                i = i + 2;
            },
            "--allowed_exts" => {
                exts = args[i + 1].to_string();
                i = i + 2;
            },
            "--convert_windows_sep" => {
                convert_windows_sep = true;
                i = i + 1;
            },
            "--help" => {
                help = true;
                break;
            },
            str => {
                error!("unknown argument {str}");
                help = true;
                break;
            }
        };
    }

    if input.is_empty() {
        println!("Input is required, use --help for usage");
        return 1;
    }

    if output.is_empty() {
        println!("Output is required, use --help for usage");
        return 1;
    }

    if help {
        println!("\
            HELP:
            --input /input/dir
            --output /output/dir
            --ignore foo,bar,baz (remove these patterns when \
                evaluating paths)
            --allowed_exts m3u,m3u8,txt (skip files without these\
                extensions)
            --convert_windows_sep (convert \\ to /)
            --help (print this help)
        ");

        return 1;
    }

    let exts = exts.split(',')
        .map(|s| s.to_string())
        .collect::<Vec<String>>();

    let targets = match get_targets_from_dir(input.clone(), exts) {
        Some(targets) => targets,
        None => {
            error!("input '{input}' is not a directory");
            return 1;
        },
    };

    let ignore = ignore
        .split(',')
        .map(|s| s.to_string())
        .collect::<Vec<String>>();

    let opts = ConvertOptions {
        targets,
        output_dir: output,
        ignore,
        convert_windows_sep
    };

    match convert_to_relative(opts) {
        Ok(i) => {
            info!("{i} lines were changed");
            0
        },
        Err(_) => 1,
    }
}

fn get_arg(arg_name: &'static str) -> Option<String> {
    let mut args = std::env::args();
    _ = args.position(|arg| arg.contains(arg_name));
    args.nth(0) 
}

fn main() -> ExitCode {
    let log_level = match get_log_level() {
        Some(level) => level,
        None => LevelFilter::Info,
    };

    match logging::init(log_level) {
        Ok(_) => { },
        Err(e) => {
            println!("Error setting logger: {e}");
            return 1.into();
        },
    };

    let args = std::env::args();
    if args.len() > 1 {
        let args = args.skip(1).collect::<Vec<String>>();
        return handle_cmd_run(args).into();
    }
    
    let main_window = MainWindow::new().unwrap();
    main_window.on_convert_to_relative(move |opts| {
        let allowed_exts = opts.allowed_exts.to_string()
            .split(',')
            .map(|s| s.to_string())
            .collect::<Vec<String>>();

        let targets = match get_targets_from_dir(
            opts.in_dir.into(),
            allowed_exts
        ) {
            Some(targets) => targets,
            None => return
        };

        let ignore = opts.ignore.to_string()
            .split(',')
            .map(|s| s.to_string())
            .collect::<Vec<String>>();

        let opts = ConvertOptions {
            targets,
            output_dir: opts.out_dir.into(),
            ignore,
            convert_windows_sep: opts.convert_windows_sep,
        };

        convert_to_relative(opts).unwrap();
    });
    
    main_window.on_select_directory_in(move || {
        select_dir("Select input directory".into()).into()
    });
    
    main_window.on_select_directory_out(move || {
        select_dir("Select output directory".into()).into()
    });
    
    main_window.run().unwrap();
    
    return 0.into();
}

struct ConvertOptions {
    targets: Vec<String>,
    output_dir: String,
    ignore: Vec<String>,
    convert_windows_sep: bool,
}

fn select_dir(title: String) -> String {
    let dir = FileDialog::new()
        .set_title(title)
        .set_directory("~/")
        .pick_folder();
    
    let dir = match dir {
        Some(dir) => {
            dir.to_str().unwrap().to_string()
        },
        None => return "".into(),
    };
    
    return dir.into()
}

fn get_targets_from_dir(
    dir: String,
    allowed_exts: Vec<String>
) -> Option<Vec<String>> {
    let path = std::path::Path::new(&dir);
    if !check_is_directory(path) {
        error!("Path '{}' is not a directory", path.display());
        return None;
    }

    let targets = path.read_dir().unwrap()
        .filter(|dir| dir.is_ok())
        .map(|dir| dir.unwrap())
        .map(|dir| dir.path().to_str().unwrap().to_string())
        .filter(|dir| {
            let path = std::path::Path::new(dir);
            path.exists() && path.is_file()
        })
        .filter(|dir| {
            let path = std::path::Path::new(dir);
            let ext = match path.extension() {
                Some(ext) => ext.to_str().unwrap().to_string(),
                None => "".into(),
            };

            allowed_exts.contains(&ext)
        })
        .collect::<Vec<String>>();
    Some(targets)
}

fn convert_to_relative(
    opts: ConvertOptions
) -> Result<i32, ()> {
    let changes_count = 0;
    let _out_dir = opts.output_dir.clone();
    let out_path = std::path::Path::new(&_out_dir);
    
    if !check_is_directory(out_path) {
        error!("Output is not a directory");
        return Err(())
    };
    
    for target in opts.targets {
        let mut target_file = match fs::File::open(target.clone()) {
            Ok(file) => file,
            Err(_) => todo!(),
        };
        
        let mut buf = Vec::new();
        match target_file.read_to_end(&mut buf) {
            Ok(_bytes_read) => { },
            Err(e) => {
                error!("Error reading target file: {e}");
            },
        };
        
        let mut new_buf: Vec<u8> = Vec::new();
        for line in buf.lines()
        .filter(|line| line.is_ok())
        .map(|line| line.unwrap()) 
        {
            let mut replaced_line = line.clone();
            opts.ignore.iter().for_each(|ignore| {
                replaced_line = replaced_line.replace(ignore, "");
            });
            
            if opts.convert_windows_sep {
                replaced_line = replaced_line.replace("\\", "/");
            }
            
            let path = std::path::Path::new(&replaced_line);
            let line = line + "\n";
            
            match path.try_exists() {
                Ok(true) => { },
                _ => {
                    new_buf.extend(line.into_bytes());
                    continue;
                },
            };
            
            let matching_part = match get_matching_part(path, out_path) {
                Some(str) => {
                    if str == "" {
                        info!("line {line} has no matching parts to {:?}",
                        out_path);
                        continue;
                    }
                    
                    str
                },
                None => { continue },
            };
            
            let back_count = match get_path_back_count(
                out_path,
                std::path::Path::new(&matching_part)
            ) {
                Some(count) => count,
                None => {
                    continue;
                },
            };
            
            let new_line = match path.to_str() {
                Some(str) => str.to_string(),
                None => {
                    error!("path string is not UTF-8");
                    new_buf.extend(line.into_bytes());
                    continue;
                },
            };
            
            let new_line = new_line.replace(&(matching_part.clone() + "/"), "");
            let new_line = "../".repeat(back_count) + &new_line;
            
            let mut bytes = new_line.clone().into_bytes();
            bytes.push(0x0A);
            new_buf.extend(bytes);
            
            debug!("line: {:?}, matching: {}, back count: {}",
            new_line, matching_part, back_count);
        }
        
        let out_file = opts.output_dir.clone() + std::path::MAIN_SEPARATOR_STR +
            target.split("/").last().unwrap();
        let out_path = std::path::Path::new(&out_file);
        
        info!("{}", out_path.display());
        
        match fs::write(out_path, new_buf) {
            Ok(_) => { },
            Err(e) => {
                error!("Error writing file {out_file}: {e}");
            },
        };
    }
    
    return Ok(changes_count);
}

fn get_matching_part(path_1: &Path, path_2: &Path) -> Option<String> {
    let mut iter_1 = path_1.iter();
    let mut iter_2 = path_2.iter();
    let mut matching_part = String::new();
    let mut parts_count = 0;
    
    loop {
        let part_1 = iter_1.next();
        let part_2 = iter_2.next();
        
        if part_1.is_none() || part_2.is_none() {
            break;
        }
        
        let part_1 = part_1.unwrap();
        let part_2 = part_2.unwrap();
        
        let part_1 = match part_1.to_str() {
            Some(str) => str,
            None => {
                error!("OsString could not be converted to &str");
                return None;
            },
        };
        
        let part_2 = match part_2.to_str() {
            Some(str) => str,
            None => {
                error!("OsString could not be converted to &str");
                return None;
            }
        };
        
        if part_1 == part_2 {
            if parts_count > 1 {
                matching_part = matching_part + std::path::MAIN_SEPARATOR_STR;
            }
            matching_part = matching_part + part_1;
            parts_count = parts_count + 1;
        } else {
            break;
        }
    }
    
    return Some(matching_part);
}

fn get_path_back_count(out_path: &Path, matching_path: &Path) -> Option<usize> {
    let mut iter = out_path.iter().rev();
    let last_matching_part = matching_path.iter()
    .rev().next().unwrap().to_str().unwrap();
    let mut back_count = 0;
    
    loop {
        let part = iter.next();
        if part.is_none() {
            return None
        }
        let part = part.unwrap();
        
        let part = match part.to_str() {
            Some(str) => str,
            None => {
                error!("OsString could not be converted to &str");
                return None;
            },
        };
        
        if part == last_matching_part {
            break;
        }
        
        back_count = back_count + 1;
    }
    
    return Some(back_count);
}

fn check_is_directory(path: &Path) -> bool {
    match path.try_exists() {
        Ok(true) => { },
        _ => {
            error!("path '{}' does not exist", path.display());
            return false
        },
    };
    
    path.is_dir()
}

